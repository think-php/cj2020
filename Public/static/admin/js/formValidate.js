// 在layui的基础上前端数据验证

var form = layui.form,
    regNum = /^\d+(.\d+)?$/; // 验证数字
// 获取要前端验证的form
var filterForm = $('button[lay-submit]').attr('lay-filter');

// 自定义验证出厂价格
form.verify({
    factory_price: function(value) {
        if (!value) {
            return '出厂价格不能为空';
        }
        if (!regNum.test(value)) {
            return '出厂价格必须为数字';
        }
        if (Number(value) == 0) {
            return '出厂价格必须大于0';
        }
    }
});
form.on('submit({' + filterForm + '})');

/*
    表单 acai2046 2020年9月4日
 */
layui.use(['form', 'layedit', 'laydate'], function() {
    var form = layui.form
        , layer = layui.layer
        , layedit = layui.layedit
        , laydate = layui.laydate;

    //秒杀时间
    laydate.render({
        elem: '#discount_start_time',
        type: 'time'
    });
    laydate.render({
        elem: '#discount_end_time',
        type: 'time'
    });
    laydate.render({
        elem: '#second_time',
        type: 'time'
    });
    laydate.render({
        elem: '.end_time',
        type: 'time'
    });
});