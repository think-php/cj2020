<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="__ADMIN_CSS__/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="__ADMIN_CSS__/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="__ADMIN_CSS__/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="__ADMIN_CSS__/animate.min.css" rel="stylesheet">
    <link href="__ADMIN_CSS__/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link rel="stylesheet" href="__LAYUIADMIN__/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="__LAYUIADMIN__/style/admin.css" media="all">
    <script src="__ADMIN_JS__/jquery.min.js?v=2.1.4"></script>
    <script src="__ADMIN_JS__/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    </script>
</head>

<body class="gray-bg">
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="ibox-content">
                    <h3>当前位置：营销中心 &raquo; <a class="layui-btn pull-right" href="__CONTROLLER__/index" style="margin-top: -10px">返回上一页 <i class="fa fa-angle-double-right"></i></a></h3>
                </div>
            </div>
        </div>
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-body">
                        <form action="__ACTION__"  class="form-horizontal layui-form" method="post" enctype="multipart/form-data">
                            <div class="layui-form-item">
                                <label class="layui-form-label" style="width: 100px;">奖项名称</label>
                                <div class="layui-input-inline">
                                    <input type="text" name="name" lay-verify="name" placeholder="请输入" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-form-mid layui-word-aux">&nbsp;</div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label" style="width: 100px;">奖品名称</label>
                                <div class="layui-input-inline">
                                    <input type="text" name="info" lay-verify="info" placeholder="请输入" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-form-mid layui-word-aux">&nbsp;</div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label" style="width: 100px;">赞助方</label>
                                <div class="layui-input-inline">
                                    <input type="text" name="sponsor" lay-verify="sponsor" value="无" placeholder="请输入" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-form-mid layui-word-aux">&nbsp;</div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label" style="width: 100px;">每一波的中奖人数</label>
                                <div class="layui-input-inline">
                                    <input type="number" name="number_of_winners" lay-verify="number_of_winners" value="1" placeholder="请输入" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-form-mid layui-word-aux">&nbsp;</div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label" style="width: 100px;">总抽奖次数</label>
                                <div class="layui-input-inline">
                                    <input type="number" name="frequency" lay-verify="frequency" value="1" placeholder="请输入" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-form-mid layui-word-aux">&nbsp;</div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label" style="width: 100px;">状态</label>
                                <div class="layui-input-block">
                                    <input type="radio" name="status" value="normal" title="正常" checked="">
                                    <input type="radio" name="status" value="hidden" title="隐藏">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button type="submit" class="layui-btn" lay-submit="" lay-filter="demo1"><i class="fa fa-check"></i>添加</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="__LAYUIADMIN__/layui/layui.all.js"></script>
<script src="__ADMIN_JS__/formValidate.js"></script>
<script>

    layui.use(['form', 'layedit', 'laydate'], function(){
        var form = layui.form
            ,layer = layui.layer;

        //自定义验证规则
        form.verify({
            name: function(value){
                if(value.length == 0){
                    return '请填写奖项名称';
                }
            },
            info: function(value){
                if(value.length == 0){
                    return '请填写奖品名称';
                }
            },
            sponsor: function(value){
                if(value.length == 0){
                    return '请填写赞助方';
                }
            },
            number_of_winners: function(value){
                if(value.length == 0){
                    return '每一波的中奖人数';
                }
            },
            frequency: function(value){
                if(value.length == 0){
                    return '请填写总抽奖次数';
                }
            }
        });

        //监听提交
        form.on('submit(demo1)', function(data){
            /*layer.alert(JSON.stringify(data.field), {
                name: '最终的提交信息'
            })*/

            return true;
        });

        $('input[name=name]').change(function() {
            let name = $('input[name=name]').val();
            $.ajax({
                async:true,//或false,是否异步
                type : "POST",
                url : './checkName',
                dataType : 'json',
                data:{
                    name:name
                },
                success : function(data) {
                    if(!data.status){
                        layer.alert('奖品名称重复')
                        $('input[name=name]').focus();
                    }
                }
            });
        });

    });
</script>
</body>
</html>