<link rel="stylesheet" href="__LAYUIADMIN__/layui/css/layui.css" media="all">
<style>
    .layui-side::-webkit-scrollbar{
        display: none;
    }
    .layui-nav-tree .layui-this, .layui-nav-tree .layui-this>a, .layui-nav-tree .layui-this>a:hover {
        background-color: orange !important;
    }
    .layui-nav-tree .layui-nav-bar{
        background-color: orange !important;
    }
</style>
<nav class="layui-side layui-side-menu" id='mydiynav'>
    <li class="nav-header layui-side-menu" style="text-align: center">
        <div class="dropdown profile-element">
            <span><img alt="image" class="img-circle" src="<if condition="$logo_url">{$logo_url}<else />__ADMIN_IMG__/logo.png</if>?random=<?php echo time()?>" width="72" /></span>
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<span class="clear">
					<span class="block m-t-xs"><strong class="font-bold"><if condition="$logo_name">{$logo_name}<else /><?php echo WEB_TITLE;?></if></strong></span>
				</span>
            </a>
        </div>
        <div class="logo-element">后台</div>
    </li>
    <ul class="layui-nav layui-nav-tree layui-bg-orange" lay-filter="test">

        <div class="layui-side" style="margin-top: 162px;">
            <li class="layui-nav-item">
                <a href="#">
                    <i class="layui-icon layui-icon-dialogue"></i>
                    <span class="nav-label">抽奖管理</span>
                </a>
                <ul class="layui-nav-child">
                    <li><a class="J_menuItem" href="__MODULE__/Awards/index">抽奖列表</a></li>
                    <li><a class="J_menuItem" href="__MODULE__/AwardsUser/index">参加人员</a></li>
                </ul>
            </li>
        </div>
    </ul>
</nav>
<script src="__LAYUIADMIN__/layui/layui.all.js"></script>