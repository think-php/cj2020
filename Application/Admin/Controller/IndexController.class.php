<?php
namespace Admin\Controller;
use Think\Controller;


class IndexController extends Controller {

    public function index(){
        define('WEB_TITLE', '后台管理');

        $this->display();
    }

    public function show(){
        layout(false);
        $this->display();
    }
}