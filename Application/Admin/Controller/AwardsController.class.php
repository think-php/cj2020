<?php
/**
 * by 淘客云 www.taokeyun.cn
 * 618大数据活动看板
 */
namespace Admin\Controller;
use Think\Controller;
use Think\Db;

header('Access-Control-Allow-Origin: *');
class AwardsController extends Controller
{
    public function _initialize()
    {

        $this->assign('WEB_TITLE',"后台管理");
        layout(false);
    }

    /**
     * 奖项列表
     */
    public function index()
    {
        $per = 15;
        if($_GET['p'])
        {
            $p=$_GET['p'];
        }else {
            $p=1;
        }
        $Awards = M('Awards');
        $count=$Awards->where('1=1')->count();
        $Page=new \Common\Model\PageModel();
        $show= $Page->show($count,$per);// 分页显示输出

        $this->assign('page',$show);

        $list = $Awards->page($p.','.$per)->order('id desc')->select();
        $this->assign('list',$list);
        $this->display();
    }
    /**
     * 编辑项目
     */
    public function edit($id)
    {
        $Awards = M('Awards');

        if(I('post.'))
        {
            $data['name'] = I('post.name');
            $data['info'] = trim(I('post.info'));
            $data['sponsor'] = I('post.sponsor');
            $data['number_of_winners'] = I('post.number_of_winners');
            $data['frequency'] = I('post.frequency');
            $data['status'] = I('post.status');
            $Awards->where('id='.$id)->save($data); // 根据条件更新记录
        }

        $data = $Awards->where("id = '$id'")->find();

        $this->assign('data',$data);

        $this->display();
    }

    /**
     * 添加项目
     */
    public function add()
    {
        if(I('post.'))
        {
            $data=array(
                'name'=>I('post.name'),
                'info'=>trim(I('post.info')),
                'sponsor'=>I('post.sponsor'),
                'number_of_winners'=>I('post.number_of_winners'),
                'frequency'=>I('post.frequency'),
                'status'=>I('post.status'),
                'create_time'=>time()
            );
            $Awards = M('Awards');
            $name = I('post.name');
            $data = $Awards->where("name = '$name'")->find();

            if($data){
                $this->error('新增奖项重复！');
            }
            if(!$Awards->create($data)){
                $this->error($Awards->getError());
            }else{
                // 验证成功
                $res=$Awards->add($data);
                if($res)
                {
                    $this->success('新增奖项成功！',U('index'));
                }else {
                    //删除文件
                    $this->error('操作失败！');
                }
            }
            die;
        }
        $this->display();
    }

    /**
     * 删除
     * @param $id
     */
    public function del($ids){
        // 删除操作
        $Awards = M('Awards');
        $res=$Awards->delete($ids);
        if($res)
        {
            echo '1';
        }else {
            echo '0';
        }
    }

    public function checkName($name){

        $Awards = M('Awards');
        $data = $Awards->where("name = '$name'")->find();
        $err['status']  = 1;
        if($data){
            $err['status']  = 0;
        }

        $this->ajaxReturn($err);
    }
}
?>