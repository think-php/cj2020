<?php
/**
 * by 淘客云 www.taokeyun.cn
 * 618大数据活动看板
 */
namespace Admin\Controller;

use Think\Controller;
use Think\Db;

header('Access-Control-Allow-Origin: *');
class AwardsUserController extends Controller
{
    public function _initialize()
    {
        $this->assign('WEB_TITLE',"后台管理");
        layout(false);
    }
    /**
     * 奖项列表
     */
    public function index()
    {
        self::userToAwardsUser();
        $per = 15;
        if($_GET['p'])
        {
            $p=$_GET['p'];
        }else {
            $p=1;
        }
        $Awards = M('AwardsUser');
        $count=$Awards->count();
        $Page=new \Common\Model\PageModel();
        $show= $Page->show($count,$per);// 分页显示输出
        $this->assign('page',$show);

        $list = $Awards->page($p.','.$per)->order('id desc')->select();
        $this->assign('list',$list);
        $this->display();
    }

    /**
     * 数据导入
     */
    public function userToAwardsUser(){
        $User = M('User');

        $AwardsUser = M('AwardsUser');

        $AwardsUserId = M('AwardsUserId');
        $AwardsUserId->find();
        //查询
        $aid = $AwardsUserId->aid;

        $userList = $User->field('uid,username, phone')->where("uid > $aid and username<>''")->order('uid asc')->select();

        $count = 0;
        foreach ($userList as $u){
            $aid = $u['uid'];
            $truename = $u['username'];
            $phone = $u['phone'];
            $res_username=$AwardsUser->where("truename='$truename' and phone='$phone'")->find();
            if($res_username){
                //重复
            }else{
                $AwardsUser->startTrans();
                $data=array(
                    'truename'=>$u['username'],
                    'phone'=>$u['phone'],
                    'awards_id'=>0,
                    'frequency_num'=>0,
                    'create_time'=>time(),
                );
                $create = $AwardsUser->create($data);

                if($create) {
                    $count += 1;
                    $AwardsUser->add($data);
                    $AwardsUser->commit();
                }else {
                    $AwardsUser->rollback();
                }
            }
        }
        $AwardsUserId->aid = $aid;
        $AwardsUserId->save();

    }

    /**
     * 编辑项目
     */
    public function edit($id)
    {
        $Awards = M('AwardsUser');

        if(I('post.'))
        {
            $data['truename'] = I('post.truename');
            $data['phone'] = trim(I('post.phone'));
            $data['awards_id'] = I('post.awards_id');
            $data['frequency_num'] = I('post.frequency_num');
            $Awards->where('id='.$id)->save($data); // 根据条件更新记录
        }

        $data = $Awards->where("id = '$id'")->find();

        $this->assign('data',$data);

        $this->display();
    }

    /**
     * 添加项目
     */
    public function add()
    {
        if(I('post.'))
        {
            $data=array(
                'truename'=>I('post.truename'),
                'phone'=>trim(I('post.phone')),
                'awards_id'=>I('post.awards_id'),
                'frequency_num'=>I('post.frequency_num'),
//                'status'=>I('post.status'),
                'create_time'=>time()
            );
            $Awards = M('AwardsUser');
            $truename = I('post.truename');
            $phone = I('post.phone');
            $data = $Awards->where("truename = '$truename' and phone = '$phone' ")->find();

            if($data){
                $this->error('新增奖项重复！');
            }
            if(!$Awards->create($data)){
                $this->error($Awards->getError());
            }else{
                // 验证成功
                $res=$Awards->add($data);
                if($res)
                {
                    $this->success('新增奖项成功！',U('index'));
                }else {
                    //删除文件
                    $this->error('操作失败！');
                }
            }
            die;
        }
        $this->display();
    }

    public function import(){
        if(I('post.')) {
            layout(false);
            if(!empty($_FILES['file']['name'])) {
                //判断文件格式
                $type=getFileExt($_FILES ['file'] ['name']);
                if($type!='.csv') {
                    $this->error('文件格式不正确，必须为xls文件！');
                }else {
                    //读取CSV文件
                    $list=readCSV($_FILES ['file'] ['tmp_name']);


                    $count=0;
                    $count_no=0;//导入失败
                    $count_all=0;
                    $err = '';
                    foreach ($list as $l) {
                        $count_all += 1;
                        //会员账号
                        $username=trim($l[0]);
                        //手机号码
                        $phone=trim($l[1]);
                        //判断会员账号是否存在
                        $AwardsUser = M('AwardsUser');
                        $res_username=$AwardsUser->where("truename='$username' and phone='$phone'")->find();

                        if($res_username) {
                            $count_no += 1;
                            $err.=$count_no.'条数据错误原因：'.$username.'-'.$phone."<br/>";
                        }else {
                            $AwardsUser->startTrans();
                            $data=array(
                                'truename'=>$username,
                                'phone'=>$phone,
                                'awards_id'=>0,
                                'frequency_num'=>0,
                                'create_time'=>time(),
                            );
                            $create = $AwardsUser->create($data);
                            if($create) {
                                $count += 1;
                                $AwardsUser->add($data);
                                $AwardsUser->commit();
                            }else {
                                $AwardsUser->rollback();
                            }
                        }
                    }
                    $success_msg='批量导入会员，<br/>共导入成功'.$count_all.'条,成功'.$count.'条,失败'.$count_no.'条<br/>'.$err;
                    $this->success($success_msg,U('index'),20);
                }
            }else {
                $this->error('请选择需要导入的会员列表文件');
            }
        }else {
            $this->display();
        }
    }

    /**
     * 删除
     * @param $id
     */
    public function del($ids){
        // 删除操作
        $Awards = M('AwardsUser');
        $res=$Awards->delete($ids);
        if($res)
        {
            echo '1';
        }else {
            echo '0';
        }
    }

    public function checkName($name){

        $Awards = M('AwardsUser');
        $data = $Awards->where("name = '$name'")->find();
        $err['status']  = 1;
        if($data){
            $err['status']  = 0;
        }

        $this->ajaxReturn($err);
    }
}
?>