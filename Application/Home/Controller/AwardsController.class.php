<?php

namespace Home\Controller;

use Think\Controller;

class AwardsController extends Controller {

	
	// 构造函数
	function _initialize() {
        layout(false);
	}

    public function index() {
        $Awards = M('Awards');
        $awards = $Awards->where("status = 'normal' and frequency - frequency_after > 0 ")->select();

        $this->assign('awards',$awards);

        $AwardsUser = M('AwardsUser');
        $shengYuNum = $AwardsUser->where('awards_id = 0')->count();

        $this->assign('shengYuNum', $shengYuNum);

        $this->display();
    }

    public function addAward(){
        if(I('post.'))
        {
            $Awards = M('Awards');
            $name = I('post.name');
            $data = $Awards->where("name = '$name'")->find();

            if($data){
                $this->ajaxReturn(2);
            }
            $data=array(
                'name'=>I('post.aw_name'),
                'info'=>trim(I('post.aw_info')),
                'sponsor'=>I('post.hp_name'),
                'number_of_winners'=>I('post.aw_num'),
                'frequency'=>I('post.aw_time'),
                'status'=>'normal',
                'frequency_after'=>0,
                'create_time'=>time()
            );

            if(!$Awards->create($data)){
                $this->ajaxReturn(0);
            }else{
                // 验证成功
                $Awards->add($data);

                $Awards = M('Awards');
                $awards = $Awards->where("status = 'normal' and frequency - frequency_after > 0 ")->select();

                $this->ajaxReturn($awards);
            }
            die;
        }else{
            die("非法请求");
        }
    }

    public function getUser($type){

        $AwardsUser = M('AwardsUser');

        $data = $AwardsUser->where("awards_id = 0")->select();

	    $this->ajaxReturn($data);
    }

    public function updateUser($uids, $awards_id, $type){
	    if($uids){
            $AwardsUser = M('AwardsUser');

            $AwardsUser->awards_id = $awards_id;

            $AwardsUser->where("id in ($uids)")->save();

            //抽奖表

            $Awards = M('Awards');

            $Awards->where("id = '$awards_id'")->setInc('frequency_after', 1);
        }

        $Awards = M('Awards');

        $data = $Awards->where("status = 'normal' and frequency - frequency_after > 0 ")->select();

        $this->ajaxReturn($data);
    }

    public function getUserRemainCount(){
        $AwardsUser = M('AwardsUser');
        echo $AwardsUser->where('awards_id = 0')->count();
    }

    public function getList(){
        $Awards = M('Awards');
        $AwardsUser = M('AwardsUser');

        $awardsData = $Awards->where("status = 'normal'")->select();

        $userData = $AwardsUser->where('awards_id > 0')->select();

        $this->assign('awardsData',$awardsData);
        $this->assign('userData',$userData);
        $this->display();
    }

    public function del(){

        M('AwardsUser')->where("1=1")->save(array(
            'awards_id' => 0
        ));

        M('Awards')->where("1=1")->save(array(
            'frequency_after' => 0
        ));

        $this->success('清除成功！',U('index'));
    }
}