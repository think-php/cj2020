/*
 Navicat Premium Data Transfer

 Source Server         : 我的本地
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : localhost:3306
 Source Schema         : cj3_2_3_utf8

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 22/12/2020 15:33:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dmooo_awards
-- ----------------------------
DROP TABLE IF EXISTS `dmooo_awards`;
CREATE TABLE `dmooo_awards`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '奖项名称',
  `info` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '奖品名称',
  `sponsor` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '赞助方',
  `number_of_winners` int(10) NULL DEFAULT NULL COMMENT '每一波的中奖人数',
  `frequency` int(10) NULL DEFAULT NULL COMMENT '总抽奖次数',
  `frequency_after` int(10) NULL DEFAULT NULL COMMENT '已抽过几次',
  `status` enum('normal','hidden') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'normal' COMMENT '状态',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dmooo_awards
-- ----------------------------
INSERT INTO `dmooo_awards` VALUES (1, '一等奖', '奥迪A6', '朱一', 10, 1, 1, 'normal', NULL);
INSERT INTO `dmooo_awards` VALUES (26, '二等奖', '自行车', '无', 5, 1, 1, 'normal', NULL);
INSERT INTO `dmooo_awards` VALUES (34, '三等奖', '洗衣服', '无', 2, 4, 1, 'normal', 1608621719);
INSERT INTO `dmooo_awards` VALUES (35, '四等奖', '口罩', '无误', 3, 1, 0, 'normal', 1608621824);

-- ----------------------------
-- Table structure for dmooo_awards_user
-- ----------------------------
DROP TABLE IF EXISTS `dmooo_awards_user`;
CREATE TABLE `dmooo_awards_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `truename` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `awards_id` int(10) NULL DEFAULT 0 COMMENT '中奖ID',
  `frequency_num` int(10) NULL DEFAULT 0 COMMENT '第几波抽奖',
  `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态:0=未领奖,1=已领奖',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 386 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '抽奖记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dmooo_awards_user
-- ----------------------------
INSERT INTO `dmooo_awards_user` VALUES (386, '张四35', '13400004475', 26, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (387, '张四36', '13400004476', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (388, '张四37', '13400004477', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (389, '张四38', '13400004478', 26, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (390, '张四39', '13400004479', 1, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (391, '张四40', '13400004480', 1, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (392, '张四41', '13400004481', 34, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (393, '张四42', '13400004482', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (394, '张四43', '13400004483', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (395, '张四44', '13400004484', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (396, '张四45', '13400004485', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (397, '张四46', '13400004486', 1, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (398, '张四47', '13400004487', 1, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (399, '张四48', '13400004488', 1, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (400, '张四49', '13400004489', 1, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (401, '张四50', '13400004490', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (402, '张四51', '13400004491', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (403, '张四52', '13400004492', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (404, '张四53', '13400004493', 34, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (405, '张四54', '13400004494', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (406, '张四55', '13400004495', 26, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (407, '张四56', '13400004496', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (408, '张四57', '13400004497', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (409, '张四58', '13400004498', 1, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (410, '张四59', '13400004499', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (411, '张四60', '13400004500', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (412, '张四61', '13400004501', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (413, '张四62', '13400004502', 26, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (414, '张四63', '13400004503', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (415, '张四64', '13400004504', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (416, '张四65', '13400004505', 1, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (417, '张四66', '13400004506', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (418, '张四67', '13400004507', 1, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (419, '张四68', '13400004508', 1, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (420, '张四69', '13400004509', 0, 0, NULL, 1608608350);
INSERT INTO `dmooo_awards_user` VALUES (421, '张四70', '13400004510', 26, 0, NULL, 1608608350);

-- ----------------------------
-- Table structure for dmooo_awards_user_id
-- ----------------------------
DROP TABLE IF EXISTS `dmooo_awards_user_id`;
CREATE TABLE `dmooo_awards_user_id`  (
  `id` int(10) NOT NULL COMMENT 'ID',
  `aid` int(10) NULL DEFAULT NULL COMMENT 'AID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dmooo_awards_user_id
-- ----------------------------
INSERT INTO `dmooo_awards_user_id` VALUES (1, 1241);

-- ----------------------------
-- Table structure for dmooo_user
-- ----------------------------
DROP TABLE IF EXISTS `dmooo_user`;
CREATE TABLE `dmooo_user`  (
  `uid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` smallint(5) UNSIGNED NOT NULL DEFAULT 1 COMMENT '组别ID',
  `expiration_date` datetime(0) NULL DEFAULT NULL COMMENT '会员到期时间',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `auth_code_id` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '授权码ID',
  `balance` float(12, 2) NULL DEFAULT 0.00 COMMENT '余额',
  `balance_user` float(12, 2) NULL DEFAULT 0.00 COMMENT '余额-用户',
  `balance_service` float(12, 2) NULL DEFAULT 0.00 COMMENT '余额-扣税',
  `balance_plantform` float(12, 2) NULL DEFAULT 0.00 COMMENT '余额-平台',
  `point` float(12, 2) NOT NULL DEFAULT 0.00 COMMENT '积分',
  `exp` float(12, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '经验 用于会员等级提升',
  `alipay_account` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付宝账号',
  `register_time` datetime(0) NOT NULL COMMENT '注册时间',
  `register_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '注册IP',
  `login_num` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '登录次数',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后登录IP',
  `openid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '第三方应用ID',
  `third_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '第三方应用类型',
  `is_freeze` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N' COMMENT '是否冻结 N未冻结 Y冻结',
  `token` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'App身份令牌',
  `token_createtime` int(11) NULL DEFAULT NULL COMMENT 'token生成时间 时间戳格式',
  `referrer_id` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '推荐人ID',
  `path` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '团队路径',
  `remark` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注姓名',
  `tb_uid` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '淘宝用户ID后6位',
  `tb_pid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '淘宝推广位',
  `tb_pid_master` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信群主推广位pid',
  `tb_rid` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '淘宝渠道关系ID',
  `is_forever` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否终生会员 Y是 N否',
  `phone_province` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机归属地-省份',
  `phone_city` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机归属地-城市',
  `is_buy` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否购物 Y是 N否',
  `is_buy_free` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否领取0元购补贴 Y是 N否',
  `is_buy_free_goods` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否购买0元购商品 Y是 N否',
  `is_agent` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否为城市代理商 Y是 N否',
  `agent_switch` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否开启代理商配置 Y是 N否 ',
  `auth_code` char(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邀请码',
  `is_share_vip` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否为分享VIP Y是 N否',
  `is_complete_info` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否完善资料 Y是 N否',
  `jd_pid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '京东pid',
  `yj_status` int(11) NOT NULL DEFAULT 1 COMMENT '1普通用户  2销售员审核中   4审核失败  5销售员',
  PRIMARY KEY (`uid`) USING BTREE,
  UNIQUE INDEX `username`(`username`, `phone`, `email`) USING BTREE,
  INDEX `idx_token`(`token`) USING BTREE,
  INDEX `idx_referrer_id`(`referrer_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1244 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dmooo_user
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
